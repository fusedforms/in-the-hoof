import dataclasses, json, logging, os, shutil, subprocess, typer
from . import __version__
from enum import Enum
from functools import partial
from math import ceil
from pathlib import Path
from rich import progress as pg
from rich.console import Group
from rich.live import Live
from rich.logging import RichHandler
from typing import TextIO
from urllib import parse as urlp
from urllib.error import HTTPError
from urllib.request import Request, urlopen, urlretrieve

FORMAT = '%(message)s'
logging.basicConfig(
    level=logging.INFO, format=FORMAT, datefmt='[%X]', handlers=[RichHandler()]
)

app = typer.Typer(pretty_exceptions_show_locals='ITH_SHOW_LOCALS' in os.environ)

per_page = 25
headers = {'User-Agent': f'in-the-hoof/{__version__}'}
sexual_rating_tags = ['safe', 'questionable', 'suggestive', 'explicit']

def fatal(msg: str):
    logging.error(msg)
    return typer.Exit(1)

def search_page(*, domain: str, apikey: str, query: str, page: int) -> dict:
    query = urlp.quote_plus(query)
    url = f'https://{domain}/api/v1/json/search/images?key={apikey}&page={page}&per_page=25&q={query}'
    with urlopen(Request(url, headers=headers)) as f:
        if not (200 <= f.status < 300):
            raise fatal(f'Got {f.status} from {domain}')
        return json.load(f)

@dataclasses.dataclass
class State:
    tags_file: TextIO
    tags: dict[str, str]
    important: list[str]
    domain: str
    destination: Path
    tags_only: bool
    spinner_progress: pg.Progress
    dl_progress: pg.Progress
    tmsu: bool

def tag_type(st: State, name: str) -> str:
    if name.startswith('artist:'):
        return 'artist'

    try:
        return st.tags[name]
    except KeyError:
        pass
    task = st.spinner_progress.add_task(f'Checking tag {name}')
    tage = (
        name
            .replace('-', '-dash-')
            .replace(':', '-colon-')
            .replace('.', '-dot-')
            .replace('+', '-plus-')
            .replace('/', '-fwslash-')
    )
    category = None
    # Tag encoding is weird and inconsistent
    for tage in [urlp.quote_plus(tage), urlp.quote(urlp.quote_plus(tage), safe='+')]:
        logging.debug(f'Looking up tag {name} as {tage}')
        url = f'https://{st.domain}/api/v1/json/tags/{tage}'
        try:
            with urlopen(Request(url, headers=headers)) as f:
                if not (200 <= f.status < 300):
                    raise fatal(f'Got {f.status} looking up {name}')
                info: dict = json.load(f)
                category = info['tag']['category'] or 'none'
                break
        except HTTPError as e:
            if e.status == 404:
                continue
            else:
                raise
    if not category:
        raise fatal(f'Could not look up details of tag {name}')
    logging.debug(f'{name} in {category}')
    st.tags_file.write(f'{name};{category}\n')
    st.tags_file.flush()
    st.tags[name] = category
    st.spinner_progress.remove_task(task)
    return category

def relevant_tags(st: State, image_json: dict) -> list[str]:
    artists = []
    important = []
    ratings = []
    characters = []
    for tag in image_json['tags']:
        match tag_type(st, tag):
            case 'artist':
                artists.append(tag)
            case 'character' | 'oc' if tag not in ['oc', 'oc only']:
                characters.append(tag)
            case 'rating':
                ratings.append(tag)
            case _ if tag in st.important:
                important.append(tag)
    if not artists:
        artists = ['[unknown]']
    if not ratings:
        raise fatal(f'Found no rating tag(s) in {image_json}')
    else:
        # Prefer the sexual rating tag, but fall back to any rating tag
        # to highlight in the filename.
        # This covers sexually-safe but (semi-)grimdark images.
        rating = next((t for t in ratings if t in sexual_rating_tags), ratings[0])
        ratings.remove(rating)
    return [*artists, rating, *important, *ratings, *characters]

def tmsu_sanitise(s: str) -> str:
    return s.replace('/', '-').replace('\\', '-').replace('=', '\\=')

def tmsu_tag(path: Path, tags: list[str]):
    logging.debug(f'Tagging {path} with {tags}')
    try:
        subprocess.run(
            ["tmsu", "tag", path, *map(tmsu_sanitise, tags)],
            check=True,
            capture_output=True,
            encoding='utf8',
        )
    except subprocess.CalledProcessError as e:
        raise fatal(f'Failed to tag {path}: {e.stderr}')

def tmsu_untag_all(path: Path):
    subprocess.run(
        ["tmsu", "untag", "-a", path],
        capture_output=True,
        encoding='utf8',
    )

def download(st: State, pid: int, image_json: dict):
    url: str = image_json['representations']['full']
    if url.startswith('/'):
        url = f'https://{st.domain}{url}'
    ext = url.rsplit('.', 1)[1]
    relevant = relevant_tags(st, image_json)
    base = ' '.join(relevant).replace('/', '-')[:231]
    fn = f'{base}.{pid}.{ext}'
    if st.tags_only:
        logging.debug(f'Would download {fn}')
        return

    logging.info(f'Downloading {fn}')
    with (st.destination / f'{fn}.json').open('w') as f:
        json.dump(image_json, f, check_circular=False, indent=2)
    task = st.dl_progress.add_task('Downloading image', total=None)
    # Download to a temp location and then move, to avoid partial files if cancelled
    tmpfn = st.destination / f'.{fn}'
    with tmpfn.open('wb') as df:
        with urlopen(Request(url, headers=headers), timeout=30) as f:
            size = int(f.info().get('Content-Length', -1))
            if size > 0:
                st.dl_progress.update(task, total=size)
            read = 0

            while True:
                block = f.read(1024*8)
                if not block:
                    break
                read += len(block)
                df.write(block)
                st.dl_progress.update(task, completed=read)

        if size >= 0 and read < size:
            raise fatal(f'Retrieval incomplete: got only {read} out of {size} bytes')

    dst = st.destination / fn
    shutil.move(tmpfn, dst)
    if st.tmsu:
        tmsu_tag(dst, relevant)
    st.dl_progress.remove_task(task)

def run_query(
    *,
    st: State,
    apikey: str,
    query: str,
    page_limit: int,
    main_progress: pg.Progress,
    live: Live,
):
    page_json = search_page(domain=st.domain, apikey=apikey, query=query, page=1)
    query_total = page_json['total']
    total = min(query_total, page_limit * 25)
    if query_total > total:
        logging.warn(f'Only fetching {total} of {query_total} total images')
    pages = ceil(total / 25)
    dl_total = 0
    with live:
        ptask = main_progress.add_task('Fetching results ', total=total)
        for i in range(1, pages+1):
            logging.debug(f'Page {i}')
            if i > 1:
                page_json = search_page(domain=st.domain, apikey=apikey, query=query, page=i)
            images = page_json['images']
            for image in images:
                pid = int(image['id'])
                existing = [
                    p
                    for p in st.destination.glob(f'*.{pid}.*')
                    if not p.name.startswith('.')
                ]
                match len(existing):
                    case 0 | 1:
                        download(st, pid, image)
                        dl_total += 1
                    case 2:
                        logging.debug(f'{existing[0].stem} exists')
                    case n:
                        logging.warn(
                            f'Found {n} files looking for existing image {pid}.\n'
                            'Assuming it exists but this is odd.'
                        )
                main_progress.advance(ptask)
    if st.tags_only:
        logging.info(f'{dl_total} images yet to be downloaded')
    else:
        logging.info(f'Downloaded {dl_total} new images')

def tmsu_retag(*, st: State, main_progress: pg.Progress, live: Live):
    paths = list(st.destination.glob('*.json'))
    tagged_total = 0
    with live:
        for json_path in main_progress.track(paths, description='Retagging files'):
            file_path = json_path.with_suffix('')
            if not file_path.exists():
                logging.warn(f'No file corresponding to {json_path}, skipping')
                continue
            with json_path.open() as f:
                image_json = json.load(f)
            relevant = relevant_tags(st, image_json)
            tmsu_untag_all(file_path)
            tmsu_tag(file_path, relevant)
            tagged_total += 1
    logging.info(f'Retagged {tagged_total} files')

class Verbosity(Enum):
    DEBUG = 'DEBUG'
    INFO = 'INFO'
    WARN = 'WARN'
    ERROR = 'ERROR'

@app.command(no_args_is_help=True)
def main(
    domain: str = typer.Argument(
        ...,
        help='Base domain eg. ponerpics.org',
        show_default=False,
    ),
    query: str = typer.Argument(
        ...,
        help='Search query, eg. my:faves',
        show_default=False,
    ),
    page_limit: int = typer.Option(
        25,
        help='Number of pages (25 images) to fetch.',
        min=1,
    ),
    tags_only: bool = typer.Option(
        False,
        help='Update tags files and skip download.',
    ),
    verbosity: Verbosity = typer.Option(
        Verbosity.INFO.value,
        help='Be noisy.',
        case_sensitive=False,
    ),
    quiet: bool = typer.Option(
        False,
        '--quiet',
        help='Be quiet [verbosity=WARN].',
    ),
    destination: Path = typer.Option(
        '.',
        help='Destination directory.',
        exists=True,
        file_okay=False,
    ),
    tmsu: bool = typer.Option(
        bool(shutil.which('tmsu')),
        help='Tag downloaded files with TMSU.',
    ),
    retag: bool = typer.Option(
        False,
        help='Ignore query and just re-tag files in destination with TMSU.'
    ),
):
    '''
    Download images from Philomena-based imageboards.
    '''
    logging.root.setLevel(getattr(logging, verbosity.value))
    if quiet:
        logging.root.setLevel(logging.WARN)

    try:
        with open(f'{domain}.apikey.txt') as f:
            apikey = f.read().strip()
    except FileNotFoundError:
        raise fatal(f'You must have a file named {domain}.apikey.txt in the current directory')

    try:
        with open('important.txt') as f:
            important = [tag.strip() for tag in f.readlines()]
    except FileNotFoundError:
        raise fatal(f'You must have a file named important.txt in the current directory')

    spinner_progress = pg.Progress(
        pg.SpinnerColumn(),
        pg.TextColumn('[progress.description]{task.description}'),
        transient=True,
    )
    dl_progress = pg.Progress(
        pg.TextColumn('[progress.description]{task.description}'),
        pg.BarColumn(),
        pg.DownloadColumn(),
        pg.TransferSpeedColumn(),
        pg.TimeRemainingColumn(),
        transient=True,
    )
    main_progress = pg.Progress(
        pg.TextColumn('[progress.description]{task.description}'),
        pg.BarColumn(),
        pg.MofNCompleteColumn(),
        pg.TimeRemainingColumn(),
    )
    live = Live(Group(spinner_progress, dl_progress, main_progress), transient=True)

    tags_file = open('tags.txt', mode='a+')
    tags_file.seek(0)
    tags = dict(
        line.strip().rsplit(';', 1)
        for line in tags_file.readlines()
    )
    st = State(
        destination=destination,
        domain=domain,
        tags_file=tags_file,
        tags=tags,
        tags_only=tags_only,
        important=important,
        spinner_progress=spinner_progress,
        dl_progress=dl_progress,
        tmsu=tmsu,
    )

    with live:
        if retag:
            tmsu_retag(st=st, live=live, main_progress=main_progress)
        else:
            run_query(
                apikey=apikey,
                live=live,
                main_progress=main_progress,
                page_limit=page_limit,
                query=query,
                st=st,
            )

if __name__ == '__main__':
    app()
