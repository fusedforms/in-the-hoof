from importlib.metadata import version

try:
    __version__ = version(__package__ or __name__)
except:
    __version__ = '0.0.1'
