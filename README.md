In The Hoof
===========

A fetcher for [Philomena-based](https://github.com/derpibooru/philomena) imageboards.

Install
-------

```sh
pip install https://gitlab.com/fusedforms/in-the-hoof/-/archive/master/in-the-hoof-master.zip
```

Example
-------

```sh
echo my-key-from-account-page > ponerpics.org.apikey.txt
echo tail bow > important.txt
in-the-hoof ponerpics.org 'my:faves, created_at:2020, ts'
```
