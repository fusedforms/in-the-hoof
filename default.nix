{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  pythonPkgs = python310Packages;
  poetry2nix = import (fetchFromGitHub {
    owner = "nix-community";
    repo = "poetry2nix";
    rev = "9fc487b32a68473da4bf9573f85b388043c5ecda";
    hash = "sha256-QfrE05P66856b1SMan69NPhjc9e82VtLxBKg3yiQGW8=";
  }) { inherit pkgs; };
  poetryArgs = {
    inherit (pythonPkgs) python;
    projectDir = ./.;
    pyproject = ./pyproject.toml;
    poetrylock = ./poetry.lock;
    preferWheels = true;
  };
  app = poetry2nix.mkPoetryApplication poetryArgs;
  env = poetry2nix.mkPoetryEnv (poetryArgs // {
    editablePackageSources = {
      "in_the_hoof" = ./.;
    };
  });
  pkgEnv = mkShell {
    buildInputs = [ env poetry ];
  };
  poetryEnv = mkShell {
    buildInputs = [ poetry ];
  };
in pkgEnv // { passthru.app = app; }
